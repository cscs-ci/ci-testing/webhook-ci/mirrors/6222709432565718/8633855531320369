set -eE

COLOR_RED='\033[0;31m'
trap 'echo -e "${COLOR_RED}Failed building/pushing container images"' ERR

if [[ $1 == "devel" ]] ; then
    DEVEL_EXT="-devel"
    set -x
elif [[ $1 == "prod" ]] ; then
    DEVEL_EXT=""
else
    echo "First argument must be devel or prod"
    exit 1
fi

ARCH=x86_64
if [[ $(uname -m) == "aarch64" ]] ; then
    ARCH=arm64
fi

PODMAN_VER="4.9.0"
# This must match the gitlab-runner version (check on the runners overview on https://gitlab.com/cscs-ci/-/runners)
HELPER_VER="alpine3.19-${ARCH}-v16.8.0"
HELPER_VER_NOARCH="alpine3.19-v16.8.0" # keep in sync with above HELPER_VER
REPO=docker.io/finkandreas/cicd-ext-k8s-container-image-builder
REPO_HELPER=docker.io/finkandreas/cicd-ext-k8s-runner-helper
