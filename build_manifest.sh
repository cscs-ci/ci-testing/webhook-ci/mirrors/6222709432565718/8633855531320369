#!/bin/bash

THIS_DIR="$(realpath $(dirname $0))"
source ${THIS_DIR}/build_common.sh

podman manifest rm $REPO:podman-${PODMAN_VER}${DEVEL_EXT} || true
podman manifest create $REPO:podman-${PODMAN_VER}${DEVEL_EXT}
for ARCH in x86_64 arm64; do
    podman manifest add $REPO:podman-${PODMAN_VER}${DEVEL_EXT} docker://$REPO:podman-${PODMAN_VER}-${ARCH}${DEVEL_EXT}
done
podman manifest push --all $REPO:podman-${PODMAN_VER}${DEVEL_EXT} docker://$REPO:podman-${PODMAN_VER}${DEVEL_EXT}

podman manifest rm ${REPO_HELPER}:${HELPER_VER_NOARCH}${DEVEL_EXT} || true
podman manifest create ${REPO_HELPER}:${HELPER_VER_NOARCH}${DEVEL_EXT}
for ARCH in x86_64 arm64; do
    podman manifest add ${REPO_HELPER}:${HELPER_VER_NOARCH}${DEVEL_EXT} docker://${REPO_HELPER}:${HELPER_VER_NOARCH}-${ARCH}${DEVEL_EXT}
done
podman manifest push --all ${REPO_HELPER}:${HELPER_VER_NOARCH}${DEVEL_EXT} docker://${REPO_HELPER}:${HELPER_VER_NOARCH}${DEVEL_EXT}
