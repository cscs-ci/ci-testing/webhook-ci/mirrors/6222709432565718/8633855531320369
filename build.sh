#!/bin/bash

THIS_DIR="$(realpath $(dirname $0))"
source ${THIS_DIR}/build_common.sh

CONTAINER_BUILDER_TAG="${REPO}:podman-${PODMAN_VER}-${ARCH}${DEVEL_EXT}"
RUNNER_HELPER_TAG="${REPO_HELPER}:${HELPER_VER_NOARCH}-${ARCH}${DEVEL_EXT}"

podman build -f Dockerfile_podman -t "$CONTAINER_BUILDER_TAG" --format=docker --build-arg PODMAN_VER="$PODMAN_VER" .
podman build -f Dockerfile_helper -t "$RUNNER_HELPER_TAG" --format=docker --build-arg VER="$HELPER_VER" .

podman push "$CONTAINER_BUILDER_TAG"
podman push "$RUNNER_HELPER_TAG"
