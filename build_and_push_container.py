#!/usr/bin/python3

import json
import multiprocessing
import os
import re
import subprocess
import sys
import urllib.request
import time

from typing import Any,List,TypedDict

import requests

class Credential(TypedDict, total=False):
    key: str
    username: str
    password: str

def run_proc(args: List[str], allow_fail: bool=False, max_tries: int=1, **kwargs: Any) -> bool:
    print(end='', flush=True)
    while max_tries>0:
        max_tries -= 1
        r = subprocess.run(args, **kwargs)
        if r.returncode == 0:
            return True
        if allow_fail == False and max_tries==0:
            r.check_returncode()
        time.sleep(3)
    return r.returncode == 0

def replace_tag_version(dockertag: str, new_version: str) -> str:
    lastColonIdx =  dockertag.rfind(":")
    if lastColonIdx == -1: return f'{dockertag}:{new_version}' # no version found --> implicitly it is `:latest`, so we just append the new_version
    if dockertag[lastColonIdx:].find('/') != -1: return f'{dockertag}:{new_version}' # we found a slash after the last colon --> must have been the port nbr and no version tag specified
    return f'{dockertag[:lastColonIdx]}:{new_version}'

def send_redis_data() -> None:
    r = requests.post(f'{os.environ["CSCS_CI_MW_URL"]}/redis/job?token={os.environ["CI_JOB_TOKEN"]}&job_id={os.environ["CI_JOB_ID"]}', json={'mode':'container-builder'})
    try:
        r.raise_for_status()
    except Exception as e:
        print(f'Failed sending data to middleware. Exception={e}')

def get_registry_hostname(dockertag: str) -> str:
    # my_image:1.0 --> hostname is docker.io and part of the official library (very unlikely)
    # finkandreas/my_image:1.0 --> hostname is docker.io (implicit as per default)
    # docker.io/finkandreas/my_image:latest --> hostname is docker.io (explicit)
    if dockertag.count('/') < 2: return 'docker.io'
    return dockertag[0:dockertag.index('/')]

if __name__ == '__main__':
    # remove default storage.conf of podman
    # It would use by default fuse-overlayfs, instead of kernel overlayfs
    # However fuse-overlayfs has a subtle bug, which makes openmpi to fail building with spack
    os.remove('/etc/containers/storage.conf')

    # copy environment to a hashmap, so we can potentially manipulate it / add items to it
    runner_env = { k:v for k,v in os.environ.items() }

    # send early so this shows up as mode: container-builder
    send_redis_data()

    # make sure
    if 'DOCKERFILE' not in runner_env:
        print("Variable 'DOCKERFILE' was not specified. Please specify it via e.g.")
        print("variables:")
        print("  DOCKERFILE: ci/docker/Dockerfile")
        sys.exit(1)
    dockerfile = os.path.join(runner_env['CI_PROJECT_DIR'], runner_env['DOCKERFILE'])
    if not os.path.exists(dockerfile):
        print(f'Could not find DOCKERFILE={runner_env["DOCKERFILE"]}. Please make sure that it points to an existing and valid Dockerfile')
        sys.exit(1)

    if 'PERSIST_IMAGE_NAME' not in runner_env:
        print("Variable 'PERSIST_IMAGE_NAME' was not specified, but it is a mandatory variable.")
        sys.exit(1)
    dockertag = runner_env['PERSIST_IMAGE_NAME']
    if  dockertag.find('../') != -1:
        print("Variable 'PERSIST_IMAGE_NAME' contains '../', which is invalid")
        sys.exit(1)

    sshkey_path = '/tmp/ssh_keyfile'
    spack_signkey_path = '/tmp/spack_sign_key.gpg'
    spack_buildcache_storage_credentials_path = '/tmp/spack_buildcache_storage_credentials'

    registry_creds = Credential()
    if 'CUSTOM_REGISTRY_USERNAME' in runner_env and 'CUSTOM_REGISTRY_PASSWORD' in runner_env:
        registry_creds['username'] = runner_env['CUSTOM_REGISTRY_USERNAME']
        registry_creds['password'] = runner_env['CUSTOM_REGISTRY_PASSWORD']

    # fetch creds
    creds = {'sshkey': Credential(),
             'spack_buildcache_storage': Credential(),
             'container_registry': Credential(),
             'spack_sign_key': Credential()}
    if 'username' in registry_creds: del creds['container_registry']
    job_token = runner_env['CI_JOB_TOKEN']
    job_id = runner_env['CI_JOB_ID']
    creds_keys = ','.join(creds.keys())
    creds_url = f'{runner_env["CSCS_CI_MW_URL"]}/credentials?token={job_token}&job_id={job_id}&creds={creds_keys}'
    creds = json.loads(urllib.request.urlopen(creds_url).read())
    if 'username' in registry_creds: creds['container_registry'] = registry_creds

    open(sshkey_path, 'w').write(creds['sshkey']["key"])
    open(spack_signkey_path, 'w').write(creds['spack_sign_key']["key"])
    open(spack_buildcache_storage_credentials_path, 'w').write(
            '[default]\n'
            f'aws_access_key_id={creds["spack_buildcache_storage"]["username"]}\n'
            f'aws_secret_access_key={creds["spack_buildcache_storage"]["password"]}')
    os.chmod(sshkey_path, 0o600)
    os.chmod(spack_signkey_path, 0o600)
    os.chmod(spack_buildcache_storage_credentials_path, 0o600)

    registry_hostname = get_registry_hostname(dockertag)
    if dockertag == 'discard':
        registry_hostname = runner_env['CSCS_REGISTRY']

    # build in memory if not explicitly disabled
    # mounting must happen before any podman command is called, so the directories are not in use already
    if runner_env.get('CSCS_BUILD_IN_MEMORY', "TRUE").lower() not in ("false", "0"):
        run_proc(['mount', '-t', 'tmpfs', 'none', '/var/tmp'])
        run_proc(['mount', '-t', 'tmpfs', 'none', '/var/lib/containers'])

    run_proc(['podman', 'login', '-u', creds["container_registry"]["username"], '--password-stdin', registry_hostname], input=creds["container_registry"]["password"].encode('utf-8'))

    # Check if image exists already and rebuild policy is set to if-not-exists
    if runner_env.get('CSCS_REBUILD_POLICY', 'if-not-exists') == 'if-not-exists':
        if run_proc(['podman', 'manifest', 'inspect', dockertag], allow_fail=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL):
            print(f'Found image with tag={dockertag} and rebuild policy is set to "if-not-exists", thus no new image is going to be created.')
            print('If you want to change the behaviour set the variable "CSCS_REBUILD_POLICY: always".')
            print('Best practice is to leave the variable at its default value and make sure that your tag name changes, whenever you need a rebuild')
            sys.exit(0)

    # test if we have permissions for dockertag by building a dummy container image and pushing to jfrog
    if dockertag != "discard":
        # build with dockertag, then retag to a different versionname to test if we have write permission, and then cleanup everything
        # This allows for 2 tests: dockertag itself is a valid name, we have write permissions (registry does not have problems at the moment)
        # podman build -t dockertag -f Dockerfile.empty
        # podman tag -t dockertag dockertag_testpermissions
        # podman push dockertag_testpermissions
        # skopeo delete docker:///dockertag_testpermissions
        # podman rmi dockertag dockertag_testpermissions
        dockertag_testpermissions = replace_tag_version(dockertag, 'testperm')
        if run_proc(['podman', 'build', '--format=docker', '-t', dockertag, '-f', '/usr/local/share/cscs/Dockerfile.empty', '.'], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, allow_fail=True) == False:
            # rerun but not with stdout/stderr redirecting to DEVNULL
            run_proc(['podman', 'build', '--format=docker', '-t', dockertag, '-f', '/usr/local/share/cscs/Dockerfile.empty', '.'], allow_fail=True)
            print(f'Cannot build image with name={dockertag}')
            sys.exit(1)
        # test permissions only on CSCS internal registry - other registries might not allow to delete the `testperm` version
        if dockertag.startswith(runner_env["CSCS_REGISTRY"]):
            print(f'Checking write permission for {dockertag}')
            run_proc(['podman', 'tag', dockertag, dockertag_testpermissions])
            if run_proc(['podman', 'push', dockertag_testpermissions], allow_fail=True, max_tries=5, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) == False:
                run_proc(['podman', 'push', dockertag_testpermissions], allow_fail=True)
                print(f'You do not have write permissions to write the image {dockertag}')
                sys.exit(1)
            # delete tag again, otherwise retrying a failed job with rebuld policy==if-not-exists behaves incorrectly - we retry up to 5 times,
            # but we also allow the job to fail, because this is nothing that the user issued
            run_proc(['skopeo', 'delete', f'docker://{dockertag_testpermissions}'], max_tries=5, allow_fail=True)
            run_proc(['podman', 'rmi', dockertag, dockertag_testpermissions], allow_fail=True, max_tries=5, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    build_command = ['podman', 'build', '--format=docker']
    build_command.extend(['-f', dockerfile])
    build_command.extend(['-t', dockertag])
    build_command.append('--layers')
    build_command.append('--pull=always')
    build_command.extend(['--isolation', 'chroot'])
    build_command.extend(['--retry=10', '--retry-delay=60s'])
    build_command.extend(['--ssh', f'default={sshkey_path}'])
    # caching is currently broken - disable it
#    build_command.extend(['--cache-from', f'{runner_env["CSCS_REGISTRY_PATH"]}/podman_cache'])
#    build_command.extend(['--cache-to', f'{runner_env["CSCS_REGISTRY_PATH"]}/podman_cache'])
#    build_command.append('--cache-ttl=2160h') # if a layer is cached but the cache is older than 2160h=90days, it will be ignored
    build_command.append(f'--volume={os.path.realpath(os.path.curdir)}:/sourcecode:ro')

    # Use the default path for the credentials. Spack (botocore) will look for it there
    build_command.append(f'--volume={spack_buildcache_storage_credentials_path}:/root/.aws/credentials:ro')
    # targt path must match the path that we are using in the script `install-spack-helper`
    # in the ci_base_containers project https://github.com/finkandreas/ci_base_containers
    build_command.append(f'--volume={spack_signkey_path}:{spack_signkey_path}:ro')
    # mount the ssh proxy config inside the container. There is no guarantee that the ssh setup of the user container
    # would include files from /etc/ssh/ssh_config.d/*.conf, but it is worth mounting it there for most use cases
    build_command.append(f'--volume=/etc/ssh/ssh_config.d/10_cscs_proxy.conf:/etc/ssh/ssh_config.d/10_cscs_proxy.conf')

    # handle build args (default one ingested by us, and other
    build_args = [f'CSCS_REGISTRY_PATH={runner_env["CSCS_REGISTRY_PATH"]}']
    num_procs = 1
    try:
        if os.path.exists('/sys/fs/cgroup/cpu.max'):
            cpumax = open('/sys/fs/cgroup/cpu.max').readline().split(' ')
            quota = int(cpumax[0])
            period = int(cpumax[1])
            num_procs = quota // period
        else:
            quota = int(open('/sys/fs/cgroup/cpu/cpu.cfs_quota_us').read())
            period = int(open('/sys/fs/cgroup/cpu/cpu.cfs_period_us').read())
            num_procs = quota // period
    except Exception:
        print("Could not get number of CPU shares. Use system CPU count as number of cores")
    if num_procs <= 1: num_procs = multiprocessing.cpu_count()
    print(f'Setting build argument NUM_PROCS={num_procs}')
    build_args.append(f'NUM_PROCS={num_procs}')
    if 'DOCKER_BUILD_ARGS' in runner_env:
        try:
            yml_docker_buildargs = json.loads(runner_env['DOCKER_BUILD_ARGS'])
            assert isinstance(yml_docker_buildargs, list)
            build_args.extend(yml_docker_buildargs)
        except Exception as e:
            print(f'''The variable "DOCKER_BUILD_ARGS" could not be parsed as a JSON array. Use e.g. DOCKER_BUILD_ARGS: '["BUILD_ARG1_NAME=ARG1_VALUE", "BUILD_ARG2_NAME=ARG2_VALUE", "ANOTHER_BUILD_ARG=some_value"]' ''')
            print(f'The exception message is {e}')
            sys.exit(1)
    for arg in build_args: build_command.extend(['--build-arg', arg])

    build_command.append('.')

    # run the build command
    try:
        run_proc(build_command)
    except subprocess.CalledProcessError:
        print(f'The container image was not built successfully')
        sys.exit(1)
    # push result to artifactory
    if dockertag != 'discard':
        try:
            run_proc(['podman', 'push', dockertag], max_tries=5)
        except subprocess.CalledProcessError:
            print(f'The container could not be pushed to the artifactory')
            sys.exit(1)
    else:
        print("Discarding to push image")

    #SECONDARY_IMAGE_NAME: docker.io/my_username/my_image:1.0
    #SECONDARY_IMAGE_USERNAME: my_username
    #SECONDARY_IMAGE_PASSWORD: $MY_STORED_CREDENTIAL
    if runner_env.get('SECONDARY_IMAGE_NAME', ''):
        print(f'Pushing also to {runner_env["SECONDARY_IMAGE_NAME"]}')
        primary_dockertag = dockertag
        dockertag = runner_env['SECONDARY_IMAGE_NAME']
        run_proc(['podman', 'tag', primary_dockertag, dockertag])
        if runner_env.get('SECONDARY_IMAGE_USERNAME', '') and runner_env.get('SECONDARY_IMAGE_PASSWORD', ''):
            print(f'Logging in to secondary registry')
            registry_hostname = get_registry_hostname(dockertag)
            run_proc(['podman', 'login', '-u', runner_env['SECONDARY_IMAGE_USERNAME'], '--password-stdin', registry_hostname], input=runner_env['SECONDARY_IMAGE_PASSWORD'].encode('utf-8'))
            try:
                run_proc(['podman', 'push', dockertag], max_tries=5)
            except subprocess.CalledProcessError:
                print(f'The container could not be pushed to the artifactory')
                sys.exit(1)
        else:
            print("Not pushing secondary image due to missing username/password")

