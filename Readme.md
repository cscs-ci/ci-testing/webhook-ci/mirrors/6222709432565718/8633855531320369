Variables:
  - `DOCKERFILE`: Mandatory, relative path to your Dockerfile (relative to your repository root)
  - `PERSIST_IMAGE_NAME`: Mandatory, the name of your container image. You can set it to `discard` and nothing will be pushed, i.e. you only care about the buildability (typically of the form `$CSCS_REGISTRY_PATH/subdirectory/image_name:${CI_COMMIT_SHORT_SHA}`)
  - `CSCS_BUILD_IN_MEMORY`: TRUE/FALSE - Only set to FALSE if your build fails due to memory constraints
  - `DOCKER_BUILD_ARGS`: JSON-array string where each entry is a string of key-value pairs, i.e. the key is your build-argument name and the value is the build-argument's value, e.g. `'["build_arg1=value1", "MY_VERSION=2.0", "BUILD_TYPE=release"]'`
  - `CSCS_REBUILD_POLICY`: always / if-not-exists - Policy when image should be rebuilt. "always" will always rebuild, "if-not-exists" will first try to check if the image `$PERSIST_IMAGE_NAME` exists and rebuild only if no such image exists already (default).
  - `SECONDARY_REGISTRY`, `SECONDARY_REGISTRY_USERNAME`, `SECONDARY_REGISTRY_PASSWORD`: Set all three variables to push the image also to an alternative registry (see also [CI doc](https://gitlab.com/cscs-ci/ci-testing/containerised_ci_doc/-/blob/main/image_registries.md?ref_type=heads))
  - `CUSTOM_REGISTRY_USERNAME`, `CUSTOM_REGISTRY_PASSWORD`: If `PERSIST_IMAGE_NAME` is not inside the default registry, then credentials for pushing have to be provided with this two variables (see also [CI doc](https://gitlab.com/cscs-ci/ci-testing/containerised_ci_doc/-/blob/main/image_registries.md?ref_type=heads))

Default exposed build-arguments (i.e. they can be used as ARG statement in your Dockerfile)
  - `CSCS_REGISTRY_PATH`: Points to the path where container images can be stored
  - `NUM_PROCS`: Number of available processors for your build job. You should use this variable when you do a parallel build `make -j$NUM_PROCS`)
Additional build-arguments can be passed via variable `DOCKER_BUILD_ARGS` (see above)

During the build stage the source code is both the build context and also bind-mounted at /sourcecode
This means that you can copy source files inside your `Dockerfile` either via
  - `COPY . /opt/another/path/for/my/sourcecode`
  - `RUN cp -a /sourcecode /opt/another/path/for/my/sourcecode`

If you do not nead the source code at all in the final container, you can also work directly with the
source code in /sourcecode and compile your software and only copy/install the final software inside the container.
Please note however that the path is mounted read-only, i.e. your software MUST support out-of-source builds.
